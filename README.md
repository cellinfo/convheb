
## how to use

```sh
graffy@graffy-ws2:~/work/convheb$ python -m venv convheb.venv
graffy@graffy-ws2:~/work/convheb$ source ./convheb.venv/bin/activate
(convheb.venv) graffy@graffy-ws2:~/work/convheb$ pip install requirements.txt
(convheb.venv) graffy@graffy-ws2:~/work/convheb$ python ./create_list_for_annex.py
```

note: cocluto can be found [here](https://git.ipr.univ-rennes1.fr/cellinfo/cocluto)

